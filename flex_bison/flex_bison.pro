#-------------------------------------------------
#
# Project created by QtCreator 2013-08-12T10:18:21
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = flex_bison
TEMPLATE = app


SOURCES += main.cpp\
        princiapl.cpp \
    scanner.cpp \
    parser.cpp \
    tablas.cpp

HEADERS  += princiapl.h \
    scanner.h \
    parser.h \
    tablas.h

FORMS    += princiapl.ui

OTHER_FILES += \
    para_compilar.txt \
    sintactico.y \
    lexico.l
