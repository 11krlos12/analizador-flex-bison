#ifndef PRINCIAPL_H
#define PRINCIAPL_H
#include <stdio.h>
#include <stdlib.h>
#include <parser.h>
#include <QTextEdit>
#include <QFile>
#include <QTextStream>

extern int yyrestart( FILE* archivo);//METODO QUE PASA EL ARCHIVO A FLEX
extern int yyparse(); //METODO QUE INICIA EL ANALISIS SINTACTICO
extern void setSalida(QTextEdit* sal); //METODO CREADO EN EL ANALIZADOR SINTACTICO PARA COMUNICAR PRINCIPAL CON EL PARSER

#include <QMainWindow>

namespace Ui {
class princiapl;
}

class princiapl : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit princiapl(QWidget *parent = 0);
    ~princiapl();
    
private slots:
    void on_pushButton_clicked();

private:
    Ui::princiapl *ui;
};

#endif // PRINCIAPL_H
