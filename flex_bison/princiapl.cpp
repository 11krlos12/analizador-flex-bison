#include "princiapl.h"
#include "ui_princiapl.h"


princiapl::princiapl(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::princiapl)
{
    ui->setupUi(this);
    ui->textEdit->setText("/*El lenguaje calcula operaciones matematicas de suma, resta y multiplicacion de numeros enteros\npara imprimir el resultado se utiliza la palabra reservada print*/\na = 1+3*5+25;\nprint(a);\nb=a+1;print(a+1);\nprint(b);\nc=a;");
}

princiapl::~princiapl()
{
    delete ui;
}

void princiapl::on_pushButton_clicked()
{
    ui->textEdit_2->setText("");//CUADRO DE TEXTO QUE MUESTRA LA SALIDA
    QFile file("temp.txt"); //SE CREA UN ARCHIVO TEMPORAL PARA COMPILARLO
    if ( file.open( file.WriteOnly ) ) { //BUFFER PARA EL TEXTO QUE SE DESEA COMPILAR
        QTextStream stream1( &file );
        stream1 << ui->textEdit->toPlainText();
    }
    const char* x = "temp.txt";
    FILE* input = fopen(x, "r" );
    yyrestart(input);//SE PASA LA CADENA DE ENTRADA A FLEX
    setSalida(ui->textEdit_2);//SE ASIGNA EL PUNTERO DE LA SALIDA PARA SER USADA POR BISON
    yyparse();//SE INICIA LA COMPILACION

}
